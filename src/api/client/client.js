var CallCommander = require('../rpc/call/call-commander');
var ConversationCommander = require('../rpc/conversation/conversation-commander');


Client = function (relation, conversationProvider, callProvider) {
    this._relation = relation;
    this._conversationProvider = conversationProvider;
    this._callProvider = callProvider;

    this._commanders = {};
    var callCommander = new CallCommander(this);
    this._commanders[callCommander.getName()] = callCommander;
    var conversationCommander = new ConversationCommander(this);
    this._commanders[conversationCommander.getName()] = conversationCommander;
};

Client.prototype.getConversationProvider = function () {
    return this._conversationProvider;
};

Client.prototype.getCallProvider = function () {
    return this._callProvider;
};

Client.prototype.getCommander = function (name) {
    return this._commanders[name];
};

Client.prototype.call = function (handler, method, args, callbackFn) {
    var me = this;
    if (callbackFn) {
        this._relation.call(handler, method, args, callbackFn);
    } else {
        return new Promise(function (resolve, reject) {
            me._relation.call(handler, method, args, {onSuccess: resolve, onError: reject});
        });
    }
};

module.exports = Client;
