var VSSON = require('../../vsson/response-callback');

ConversationCommander = function (client) {
    this._client = client;
};

ConversationCommander.NAME = "conversation";

ConversationCommander.prototype.getName = function () {
    return ConversationCommander.NAME;
};


ConversationCommander.prototype.getConversations = function () {
    this._client.call(ConversationCommander.NAME, "getConversations", [], VSSON.getResponseCallback(arguments));
};

ConversationCommander.prototype.create = function (conversationInfo) {
    this._client.call(ConversationCommander.NAME, "create", [conversationInfo], VSSON.getResponseCallback(arguments));
};

ConversationCommander.prototype.addMessage = function (id, message) {
    this._client.call(ConversationCommander.NAME, "addMessage", [id, message], VSSON.getResponseCallback(arguments));
};

module.exports = ConversationCommander;