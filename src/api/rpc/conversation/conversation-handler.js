var Constants = require('../../../util/datatype');

var ConversationHandler = function(vs) {
    this._vs = vs;
};

ConversationHandler.NAME = "conversation";

ConversationHandler.prototype.created = function (conversationInfo){
    var conversation = this._vs.getClient().getConversationProvider().create(conversationInfo);
    if (conversation) {
        conversation.getConversationEventListener().created(conversationInfo);
        this._vs.getBus().post(Constants.IConversationEvent.created, [conversation]);
    }
};

ConversationHandler.prototype.addedMessage = function(id, message) {
    var conversation = this._vs.getClient().getConversationProvider().get(id);
    if (conversation) {
        conversation.getConversationEventListener().addedMessage(message);
        this._vs.getBus().post(Constants.IConversationEvent.addedMessage, [id, message]);
    }
};

ConversationHandler.prototype.getName = function(){
    return ConversationHandler.NAME;
};

// ConversationHandler.prototype['created'] = VS.ConversationHandler.prototype.created;
// ConversationHandler.prototype['addedMessage'] = VS.ConversationHandler.prototype.addedMessage;

module.exports = ConversationHandler;