var Constants = require('../../../util/datatype');

var CallHandler = function (vs) {
    this._vs = vs;
};

CallHandler.NAME = "call";

CallHandler.prototype.getName = function () {
    return CallHandler.NAME;
};

CallHandler.prototype.invite = function (callInfo) {
    this._vs.getClient().getCallProvider().invite(callInfo, this._vs._options);
};

CallHandler.prototype.setConnectionInfo = function (callId, connectionInfo) {
    var call = this._vs.getClient().getCallProvider().get(callId);
    call.updateConnectionInfo(connectionInfo);
    call.startConnectivity().then(localConnectionInfo => {
        console.log("Send local ConnectionInfo ", localConnectionInfo);
        if (localConnectionInfo) {
            this._vs.getClient().call("call", Constants.ServerMethod.setConnectionInfo, [callId, localConnectionInfo]);
        }
    }).catch(e => {
        console.error("PIZDA", e);
    });
};

//@deprecated
CallHandler.prototype.setTrackInfo = function (callId, tracksInfo) {
}

CallHandler.prototype.updateTrackInfo = function (callId, participant, tracksInfo) {
    var call = this._vs.getClient().getCallProvider().get(callId);
    //todo save track info and participant in call
    this._vs.getBus().post(Constants.APIEvent.trackInfo, [call._callInfo, participant, tracksInfo]);
}


CallHandler.prototype.addExtraConnectionInfo = function (callId, extraSessionDescription) {
    var call = this._vs.getClient().getCallProvider().get(callId);
    call.addExtraSessionDescription(extraSessionDescription);
};

CallHandler.prototype.left = function (callId, participantLeaveEvent) {
    var call = this._vs.getClient().getCallProvider().get(callId);
    //todo update callInfo who left
    if (call) {
        this._vs.getBus().post(Constants.APIEvent.left, [call._callInfo, participantLeaveEvent]);
    }
};

CallHandler.prototype.addedParticipant = function (callId, callParticipant) {
    var call = this._vs.getClient().getCallProvider().get(callId);
    call.addParticipant(callParticipant);
    this._vs.getBus().post(Constants.APIEvent.addedParticipant, [call._callInfo, callParticipant]);

};

CallHandler.prototype.removedParticipant = function (callId, callParticipant) {
    var call = this._vs.getClient().getCallProvider().get(callId);
    if (call) {
        call.removeParticipant(callParticipant);
        this._vs.getBus().post(Constants.APIEvent.removedParticipant, [call._callInfo, callParticipant]);
    }
};

CallHandler.prototype.joined = function (callId, participant) {
    var call = this._vs.getClient().getCallProvider().get(callId);
    call.joinedParticipant(participant);
    this._vs.getBus().post(Constants.APIEvent.joined, [call._callInfo, participant]);
    //todo duplicate code, refactor when any worker have changes execute it
    call.startConnectivity().then(localConnectionInfo => {
        console.log("Send local ConnectionInfo after joined event ", localConnectionInfo);
        if (localConnectionInfo) {
            this._vs.getClient().call("call", Constants.ServerMethod.setConnectionInfo, [callId, localConnectionInfo]);
        }
    }).catch(e => {
        console.error("PIZDA after joined event", e);
    });

};

CallHandler.prototype.finished = function (callId) {
    this._vs.getClient().getCallProvider().destroy(callId);
};

module.exports = CallHandler;
