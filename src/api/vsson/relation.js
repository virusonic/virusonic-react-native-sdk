var Websocket = require('./websocket');

function Relation(uri) {
    this._uri = uri;
    this._messageListeners = [];
    this._relationListeners = [];
    this._waitingMessages = {};
    this._countMsg = 1;
    this._request = 30;
    this._response = 31;
    this._error = 32;
    this._r = false;
    this._arrId = [0];

    this._wsConnection = new Websocket(this._uri, this._incomingMessage.bind(this));
}

Relation.prototype = {

    connect: function () {
        this._wsConnection.connect();
    },

    call: function (handler, method, args, responseCallbackFn) {
        console.log("Handler" , handler);
        console.log("Method" , method);
        console.log("Args" , JSON.stringify(args));

        var me = this;
        var data = {handler: handler, method: method, args: args};
        var message = me._getPacMessage(me._request, me._r, data);
        me._wsConnection.write(message);
        if (responseCallbackFn) {
            message.responseCallback = responseCallbackFn;
            me._waitingMessages[message.id] = message;
        }
    },

    close: function () {
        if (this._wsConnection) {
            this._wsConnection.close();
        }
    },

    addEventListener: function(type, listener) {
        this._wsConnection.addEventListener(type, listener);
    },

    addMessageListener: function (messageListener) {
        this._messageListeners.push(messageListener);
    },

    addRelationListener: function(relationListener) {
        this._wsConnection.addRelationListener(relationListener);
    },

    _incomingMessage: function (data) {
        var me = this;
        var dataMessage = JSON.parse(data);
        if (dataMessage.t === me._response || dataMessage.t === me._error) {
            dataMessage.parentMessage = this._waitingMessages[dataMessage.id];
            delete this._waitingMessages[dataMessage.id];
        }
        this._messageListeners.forEach(function (messageListener) {
            messageListener.onMessage(this, dataMessage);
        });
    },

    _getPacMessage: function (frameType, r, data) {
        return {
            t: frameType,
            sn: this._countMsg++,
            r: r,
            id: this._getNewIdPack(),
            data: data
        }
    },

    _getNewIdPack: function () {
        var newid = Math.round(Math.random() * 65535);
        if (this._arrId.indexOf(newid) === -1) {
            this._arrId.push(newid);
            return newid;
        }
        else {
            return this._getNewIdPack();
        }
    }

};

module.exports = Relation;
