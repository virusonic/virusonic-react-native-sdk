HandlerManager = function() {
    this._handlers = {};
};

HandlerManager.prototype.register = function(handler) {
    this._handlers[handler.getName()] = handler;
};

HandlerManager.prototype.getHandler = function(name) {
    return this._handlers[name];
};

module.exports = HandlerManager;