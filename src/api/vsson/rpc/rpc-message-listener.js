var RPCMessageListener = function (handlerManager) {
    this._request = 30;
    this._response = 31;
    this._error = 32;
    this._handlerManager = handlerManager;
};

RPCMessageListener.prototype.onMessage = function (relation, dataMessage) {
    console.log("Incoming DataMessage" , JSON.stringify(dataMessage));

    if (dataMessage.t === this._request) {
        var methodExecution = dataMessage.data;
        var handler = this._handlerManager.getHandler(methodExecution.handler);
        if (handler) {
            handler[methodExecution.method].apply(handler, methodExecution.args);
        }
    } else if (dataMessage.t === this._response && dataMessage.parentMessage) {
        var responseCallback = dataMessage.parentMessage.responseCallback;
        if (responseCallback && responseCallback["onSuccess"]) {
            responseCallback["onSuccess"].apply(responseCallback, [dataMessage.data]);
        }
    } else if (dataMessage.t === this._error && dataMessage.parentMessage) {
        var responseCallback = dataMessage.parentMessage.responseCallback;
        if (responseCallback && responseCallback["onError"]) {
            responseCallback["onError"].apply(responseCallback, [dataMessage.data]);
        }
    }
};

module.exports = RPCMessageListener;
