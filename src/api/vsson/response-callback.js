var ResponseCallback = function(onSuccess, onError){
    return {onSuccess: onSuccess, onError : onError};
};

var getResponseCallback = function(args){
    if (args && args.length > 0 && typeof args[args.length - 1].onSuccess === 'function') {
        return args[args.length - 1];
    }
    return undefined;
};

module.exports = {
    ResponseCallback: ResponseCallback,
    getResponseCallback: getResponseCallback
};