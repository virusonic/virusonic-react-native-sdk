var RelationListener = function(onConnect, onDisconnect){
    return {onConnect: onConnect, onDisconnect : onDisconnect};
};

module.exports= RelationListener;