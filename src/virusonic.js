var HandlerManager = require('./api/vsson/rpc/handler-manager');
var RPCMessageListener = require('./api/vsson/rpc/rpc-message-listener');

var Client = require('./api/client/client');
var Relation = require('./api/vsson/relation');
var RelationListener = require('./api/vsson/relation-listener');

var CallHandler = require('./api/rpc/call/call-handler');
var ConversationHandler = require('./api/rpc/conversation/conversation-handler');

var CallProvider = require('./call/call-provider');
var ConversationProvider = require('./conversation/conversation-provider');

var AuthManager = require('./auth/auth-manager');
var UserManager = require('./user/user-manager');
var ConversationManager = require('./conversation/conversation-manager');
var CallManager = require('./call/call-manager');

var Constants = require('./util/datatype');

var EventBus = require('./util/event-bus');

function VS(uri, options) {
    var me = this;
    me._uri = uri;
    me._options = options;
    me._bus = new EventBus();
}

VS.prototype.connect = function () {
    var me = this;
    me._handlerManager = new HandlerManager();
    this._relation = new Relation(this._uri);
    this._relation.addMessageListener(new RPCMessageListener(this._handlerManager));
    this._relation.addRelationListener(new RelationListener(function () {
            me._bus.post(Constants.RelationEvent.connected, this);
        },
        function (event) {
            me._bus.post(Constants.RelationEvent.disconnected, [event]);
        }));
    this._relation.connect();

    this._client = new Client(this._relation, new ConversationProvider(this), new CallProvider(this));

    this._handlerManager.register(new CallHandler(this));
    this._handlerManager.register(new ConversationHandler(this));

    this._authManager = new AuthManager(this._client, this._options);
    this._userManager = new UserManager(this._client, this._options);
    this._conversationManager = new ConversationManager(this._client, this._options);
    this._callManager = new CallManager(this, this._options);

    const LocalMediaManager = require('./media/media-manager');

    this._localMediaManager = new LocalMediaManager();
};

VS.prototype.getBus = function () {
    return this._bus;
};

VS.prototype.getClient = function () {
    return this._client;
};

VS.prototype.getHandlerManager = function () {
    return this._handlerManager;
};

VS.prototype.getAuthManager = function () {
    return this._authManager;
};

VS.prototype.getUserManager = function () {
    return this._userManager;
};

VS.prototype.getConversationManager = function () {
    return this._conversationManager;
};

VS.prototype.getCallManager = function () {
    return this._callManager;
};

VS.prototype.getMediaManager = function () {
    return this._localMediaManager;
};


VS.prototype.close = function () {
    delete this._callManager;
    delete this._conversationManager;
    delete this._userManager;
    delete this._authManager;

    delete this._handlerManager;

    this._relation?.close();

    delete this._client;
};

// window['VS'] = VS;
// VS.prototype['register'] = VS.prototype.register;
//
// VS.prototype['getHandlerManager'] = VS.prototype.getHandlerManager;
//
// VS.prototype['getAuthManager'] = VS.prototype.getAuthManager;
// VS.prototype['getUserManager'] = VS.prototype.getUserManager;
// VS.prototype['getConversationManager'] = VS.prototype.getConversationManager;
// VS.prototype['getCallManager'] = VS.prototype.getCallManager;
//
// VS.prototype['disconnect'] = VS.prototype.disconnect;

module.exports = VS;
