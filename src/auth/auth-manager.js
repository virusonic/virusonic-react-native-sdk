var EventBus = require('../util/event-bus');
var VSSON = require('../api/vsson/response-callback');
var Constants = require('../util/datatype');

var AuthManager = function AuthManager(client, options) {
    EventBus.apply(this, arguments);
    this._client = client;
    this._options = options;
    this._loggedUser = {};
};

AuthManager.prototype = Object.create(EventBus.prototype);
AuthManager.prototype.constructor = AuthManager;

AuthManager.prototype.getLoggedUser = function () {
    return this._loggedUser;
};

AuthManager.prototype.auth = function (name, pwd, deviceUdid) {
    var me = this;
    return new Promise(function (resolve, reject) {
        me._client.call("auth", Constants.ServerMethod.auth, [name, pwd, {attributes: {}, deviceUdid, apiVersion: "1.0.2"}])
            .then(function (data) {
                if (data.status === "10") {
                    reject();
                } else {
                    me._loggedUser = data.user;
                    resolve(data.user);

                    let callProvider = me._client.getCallProvider();
                    callProvider.processCurrentCalls(data.calls, me._options);
                }
            }).catch(e => {
            reject(e);
        });
    });
};

AuthManager.prototype.changeSettings = function (lUser, visibleName, status, textStatus) {
    var me = this;
    me._client.call("main", Constants.ServerMethod.changeSettings, [lUser.username, visibleName, status, textStatus]);
};

// VS['AuthManager'] = VS.AuthManager;
// VS.AuthManager.prototype['auth'] = VS.AuthManager.prototype.auth;
// VS.AuthManager.prototype['changeSettings'] = VS.AuthManager.prototype.changeSettings;
// VS.AuthManager.prototype['getLoggedUser'] = VS.AuthManager.prototype.getLoggedUser;


module.exports = AuthManager;
