var EventBus = require('../util/event-bus');
var ConversationCommander = require('../api/rpc/conversation/conversation-commander');
var VSSON = require('../api/vsson/response-callback');
var Utils = require('../util/util');

var ConversationManager = function ConversationManager(client, options) {
    this._client = client;
    this._options = options;
};


ConversationManager.prototype.getConversations = function (callbackFn) {
    var me = this;
    this._client.getCommander(ConversationCommander.NAME).getConversations(new VSSON.ResponseCallback(function (data) {
        data.forEach(function (conversationInfo) {
            var conversations = [];
            var conversation = me._client.getConversationProvider().create(conversationInfo);
            if (conversation) {
                conversation.getConversationEventListener().created(conversationInfo);
                conversations.push(conversation);
            }
            if (callbackFn) {
                callbackFn(conversations);
            }
        });
    }));
};

ConversationManager.prototype.create = function (conversationInfo) {
    conversationInfo.id = Utils.getUUID();
    if (!conversationInfo.name) {
        conversationInfo.name = "";
        conversationInfo.participants.forEach(function (participant) {
            conversationInfo.name += participant.username + ",";
        });
        if (conversationInfo.name) {
            conversationInfo.name = conversationInfo.name.substr(0, conversationInfo.name.length - 1);
        }
    }
    var conversation = this._client.getConversationProvider().create(conversationInfo);
    this._client.getCommander(ConversationCommander.NAME).create(conversationInfo);
    return conversation;
};

ConversationManager.prototype.get = function (id) {
    return this._client.getConversationProvider().get(id);
};

ConversationManager.prototype.getByUsername = function (username) {
    return this._client.getConversationProvider().getByUsername(username);
};

ConversationManager.prototype.addMessage = function (id, message) {
    var conversation = this._client.getConversationProvider().get(id);
    if (conversation) {
        conversation.getConversationController().addMessage(message);
    }
};

ConversationManager.prototype.getHistory = function (id) {
    var conversation = this._client.getConversationProvider().get(id);
    if (conversation) {
        return conversation.history;
    }
};

// VS['ConversationManager'] = VS.ConversationManager;
// VS.ConversationManager.prototype['create'] = VS.ConversationManager.prototype.create;
// VS.ConversationManager.prototype['get'] = VS.ConversationManager.prototype.get;
// VS.ConversationManager.prototype['getByUsername'] = VS.ConversationManager.prototype.getByUsername;
// VS.ConversationManager.prototype['addMessage'] = VS.ConversationManager.prototype.addMessage;
// VS.ConversationManager.prototype['getHistory'] = VS.ConversationManager.prototype.getHistory;

module.exports = ConversationManager;
