ConversationEventListener = function (conversation) {
    this._conversation = conversation;
};

ConversationEventListener.prototype.created = function (conversationInfo) {

};

ConversationEventListener.prototype.addedMessage = function (message) {
    this._conversation.history.push(message);
};


module.exports = ConversationEventListener;