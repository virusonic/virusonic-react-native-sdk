var ConversationEventListener = require('./conversation-event-listener');
var ConversationController = require('./conversation-controller');

var Conversation = function(client){
    this.id = null;
    this.participants = null;
    this.attributes = null;
    this.name = null;
    this.status = null;
    this.history = [];

    this._conversationEventListener = new ConversationEventListener(this);
    this._conversationController = new ConversationController(this, client);
};

Conversation.prototype.getConversationEventListener = function() {
    return this._conversationEventListener;
};

Conversation.prototype.getConversationController = function() {
    return this._conversationController;
};

module.exports = Conversation;