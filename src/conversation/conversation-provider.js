var Storage = require('../util/storage');
var Conversation = require('./conversation');

var ConversationProvider = function ConversationProvider(virusonic) {
    this._virusonic = virusonic;
    this._storage = new Storage();
};

ConversationProvider.prototype.get = function (id) {
    return this._storage.get(id);
};

ConversationProvider.prototype.getByUsername = function (username) {
    var conversation = null;
    this._storage.forEach(function (conv) {
        if (conv.participants.length == 1 &&
            conv.participants[0].username == username) {
            conversation = conv
        }
    });
    return conversation;
};


ConversationProvider.prototype.create = function (conversationInfo) {
    var conversation = new Conversation(this._virusonic.getClient());
    conversation.id = conversationInfo.id;
    conversation.participants = conversationInfo.participants;
    conversation.attributes = conversationInfo.attributes;
    conversation.name = conversationInfo.name;

    this._storage.add(conversation);

    return conversation;
};

module.exports = ConversationProvider;