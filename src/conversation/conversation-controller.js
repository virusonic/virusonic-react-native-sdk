var ConversationCommander = require('../api/rpc/conversation/conversation-commander');

ConversationController = function (conversation, client) {
    this._conversation = conversation;
    this._client = client;

};

ConversationController.prototype.addMessage = function (message) {
    this._conversation.history.push(message);
    this._client.getCommander(ConversationCommander.NAME).addMessage(this._conversation.id, message);
};

module.exports = ConversationController;