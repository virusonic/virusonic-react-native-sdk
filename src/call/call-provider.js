var Storage = require('../util/storage');
var Call = require("./call");
var Constants = require('../util/datatype');

var CallProvider = function CallProvider(vs) {
    this._vs = vs;
    this._storage = new Storage();
};

CallProvider.prototype.get = function (id) {
    return this._storage.get(id);
};

//todo create update method for callInfo
CallProvider.prototype.create = function (callInfo, options, incoming) {
    var me = this;
    if (me._storage.get(callInfo.id)) {
        throw new Error("Call with " + callInfo.id + " already exist");
    }
    let loggedUser = me._vs.getAuthManager().getLoggedUser();
    const call = new Call(callInfo, options, loggedUser, incoming);
    me._storage.add(call);

    call.register(Constants.CallEvent.onIceCandidate, function (idSession, username, candidate) {
        const callManager = me._vs.getCallManager();
        if (username) {
            const usersExtraSDPInfo = {};
            usersExtraSDPInfo[username] = {"iceCandidates": [candidate]};
            callManager.addExtraConnectionInfo(idSession, {usersExtraSDPInfo: usersExtraSDPInfo});
        } else {
            callManager.addExtraConnectionInfo(idSession, {"extraSDPInfo": {"iceCandidates": [candidate]}});
        }
    });
    call.register(Constants.CallEvent.onTrack, (username, event) => {
        me._vs.getBus().post(Constants.APIEvent.onTrack, [call._callInfo, username, event]);
    });

    call.register(Constants.CallEvent.trackInfo, (participant, tracksInfo) => {
        me._vs.getBus().post(Constants.APIEvent.trackInfo, [call._callInfo, participant, tracksInfo]);
    });

    call.register(Constants.CallEvent.connectionState, (username, event) => {
        me._vs.getBus().post(Constants.APIEvent.connectionState, [call._callInfo, username, event]);
    });

    call.register(Constants.CallEvent.dead, (username, event) => {
        console.log("CallEvent dead username=" + username + ", event=" + event);
        //todo add new state for participant and hide it locally if connection dead
    });

    return call;
};

CallProvider.prototype.processCurrentCalls = function (serverCalls, options) {
    const me = this;
    if (serverCalls) {
        me._storage.forEach(call => {
            let finded = false;
            serverCalls.forEach(serverCall => {
                if (serverCall.id === call.id) {
                    finded = true;
                }
            })
            if (!finded) {
                me.destroy(call.id);
            }
        });
        if (serverCalls.length > 0) {
            //todo implement save all calls and get call state and save call participant state
            let callInfo = serverCalls[0];

            me.invite(callInfo, options);
        }
    }
}

CallProvider.prototype.invite = function (callInfo, options) {
    var me = this;
    let call = me.get(callInfo.id);
    if (!call) {
        call = me.create(callInfo, options, true);

        me._vs.getBus().post(Constants.APIEvent.invite, [callInfo]);

        //todo check need/not need update connection info (may be if p2p ned create new peerconnection)
        call.updateConnectionInfo(callInfo.connectionInfo);
    } else {
        //todo update callInfo (maybe anybody left of join)
        console.log("Call with id " + call.id + " already exist, ignore info");
    }
}

CallProvider.prototype.destroy = function (callId) {
    var me = this;
    let call = me.remove(callId);
    if (call) {
        console.log("Destroy call with id=" + callId);
        call.destroy();
        call.post(Constants.CallEvent.finished);
        me._vs.getBus().post(Constants.APIEvent.finished, [call._callInfo]);
    }
    return call;
}

CallProvider.prototype.remove = function (id) {
    return this._storage.remove(id);
};

module.exports = CallProvider;
