var EventBus = require('../util/event-bus');
var Util = require('../util/util');
var Constants = require('../util/datatype');
var VSSON = require('../api/vsson/response-callback');

var CallManager = function CallManager(vs, options) {
    EventBus.apply(this, arguments);
    this._vs = vs;
    this._client = vs._client;
    this._options = options;
    this._outgoingCallId = undefined;
};

CallManager.prototype = Object.create(EventBus.prototype);
CallManager.prototype.constructor = CallManager;

CallManager.prototype.invite = function (username, stream, attributes) {
    var me = this;
    if (this._outgoingCallId) {
        throw new Error("Sdk can not have more one ougoing call, current outgoing call id = " + this._outgoingCallId);
    }

    this.outgoingCallId = Util.getUUID();

    var callInfo = {
        id: this.outgoingCallId,
        p2p: true,
        attributes,
        participants: [{
            username: username,
            role: 20
        }]
    };
    var call = me._client.getCallProvider().create(callInfo, me._options, false);

    call.register(Constants.CallEvent.finished, function () {
        me._outgoingCallId = undefined;
    });

    return call.startConnectivity(stream).then((connectionInfo) => {
        //todo create method for update connection info
        callInfo.connectionInfo = connectionInfo;
        //todo get callInfo from server after create it
        return me._client.call("call", Constants.ServerMethod.invite, [callInfo])
            .then(() => {
                return callInfo;
            })
    });
};

CallManager.prototype.addParticipant = function (id, username) {
    const me = this;
    const call = me._client.getCallProvider().get(id);

    //todo add roles in library
    const participant = {
        username: username,
        role: 20
    };

    me._client.call("call", Constants.ServerMethod.addParticipant, [id, participant]);
};

CallManager.prototype.removeParticipant = function (id, username) {
    const me = this;
    const call = me._client.getCallProvider().get(id);
    //todo get participant by username
    const participant = {
        username: username,
        role: 20
    };

    me._client.call("call", Constants.ServerMethod.removeParticipant, [id, participant]);
};

CallManager.prototype.join = function (id, stream) {
    const me = this;
    me._stream = stream;
    const call = me._client.getCallProvider().get(id);

    return call.startConnectivity(stream).then((connectionInfo) => {
        me._client.call("call", Constants.ServerMethod.join, [call.id, connectionInfo]);
    });
};

CallManager.prototype.addExtraConnectionInfo = function (id, extraConnectionInfo) {
    var me = this;
    me._client.call("call", Constants.ServerMethod.addExtraConnectionInfo, [id, extraConnectionInfo]);
};

CallManager.prototype.muteAudio = function (id, mute) {
    var me = this;
    var call = me._client.getCallProvider().get(id);
    if (call) {
        const tracksInfo = call.muteAudio(mute);
        me._client.call("call", Constants.ServerMethod.setTrackInfo, [id, tracksInfo]);
    }
}

CallManager.prototype.muteVideo = function (id, mute) {
    var me = this;
    var call = me._client.getCallProvider().get(id);
    if (call) {
        const tracksInfo = call.muteVideo(mute);
        me._client.call("call", Constants.ServerMethod.setTrackInfo, [id, tracksInfo]);
    }
}

CallManager.prototype.leave = function (id) {
    var me = this;
    var call = me._client.getCallProvider().get(id);
    if (call) {
        var leaveParticipantEvent = {
            status: 30
        };
        me._client.call("call", Constants.ServerMethod.leave, [call.id, leaveParticipantEvent]);

        me._client.getCallProvider().destroy(call.id);
    }
};

//todo may be delete bacause calls received on authorization
CallManager.prototype.getCalls = function () {
    var me = this;
    var args = new VSSON.ResponseCallback(function (data) {
        if (data && data.length > 0) {
            let callInfo = data[0];

            let callProvider = me._client.getCallProvider();
            callProvider.invite(callInfo, this._vs._options);
        }
    });
    me._client.call("call", Constants.ServerMethod.getCalls, [], args);
};

// VS['CallManager'] = VS.CallManager;
// VS.CallManager.prototype['getCalls'] = VS.CallManager.prototype.getCalls;
// VS.CallManager.prototype['join'] = VS.CallManager.prototype.join;
// VS.CallManager.prototype['invite'] = VS.CallManager.prototype.invite;
// VS.CallManager.prototype['leave'] = VS.CallManager.prototype.leave;

module.exports = CallManager;
