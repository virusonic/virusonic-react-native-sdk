var Storage = function() {
    this._elements = {};
};

Storage.prototype = {

    add: function (call) {
        this._elements[call.id] = call;
    },

    remove: function (id) {
        var element = this._elements[id];
        if (element) {
            delete this._elements[id];
        }
        return element;
    },

    get: function (id) {
        return this._elements[id];
    },

    forEach: function (fn) {
        for (var id in this._elements){
            fn(this._elements[id]);
        }
    }
};

module.exports = Storage;
