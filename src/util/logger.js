'use strict';
var loglevel = 'DEBUG';
var showTime = false;
var countMessage = 1;

var logger= (function(){
    function getTime(){
        var today = new Date();
        // get hours, minutes and seconds
        var hh = today.getUTCHours().toString();
        var mm = today.getUTCMinutes().toString();
        var ss = today.getUTCSeconds().toString();
        var ms = today.getUTCMilliseconds().toString();

        // Add leading '0' to see 14:08:06.001 instead of 14:8:6.1
        hh = hh.length == 1 ? "0" + hh : hh;
        mm = mm.length == 1 ? "0" + mm : mm;
        ss = ss.length == 1 ? "0" + ss : ss;
        ms = ms.length == 1 ? "00" + ms : ms.length == 2 ? "0" + ms : ms;

        // return time
        return "UTC " + hh + ':' + mm + ':' + ss + '.' + ms;
    }

    return {
        error : function(message) {
            if (loglevel === 'DEBUG' || loglevel === 'ERROR') {
                if (typeof(message) === 'object'){
                    (showTime) ? console.error(countMessage+' - '+getTime()+' [ERROR]: ') : console.error(countMessage+' - [ERROR]: ');
                    console.error(message);
                }
                else {
                    (showTime) ? console.error(countMessage+' - '+getTime() + ' [ERROR]: ' + message) : console.error(countMessage+' - [ERROR]: ' + message);
                }
                countMessage++;
            }
        },

        debug : function(message) {
            if (loglevel === 'DEBUG') {
                if (typeof(message) === 'object'){
                    (showTime) ? console.debug(countMessage+' - '+getTime()+' [DEBUG]: ') : console.debug(countMessage+' - [DEBUG]: ');
                    console.debug(message);
                }
                else {
                    (showTime) ? console.debug(countMessage+' - '+getTime() + ' [DEBUG]: ' + message) : console.debug(countMessage+' - [DEBUG]: ' + message);
                }
                countMessage++;
            }
        },

        info : function(message) {
            if (loglevel === 'DEBUG' || loglevel === 'ERROR' || loglevel === 'INFO') {
                if (typeof(message) === 'object'){
                    (showTime) ? console.info(countMessage+' - '+getTime()+' [INFO]: ') : console.info(countMessage+' - [INFO]: ');
                    console.info(message);
                }
                else {
                    (showTime) ? console.info(countMessage+' - '+getTime() + ' [INFO]: ' + message) : console.info(countMessage+' - [INFO]: ' + message);
                }
                countMessage++;
            }
        }
    }
})();
