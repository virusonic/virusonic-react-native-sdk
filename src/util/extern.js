
function Options(){
    this.host = null;
    this.port = null;
    this.STUN_SERVERS = null;
    this.TURN_SERVERS = null;
    this.webrtc = null;
}

function TURN(){
    this.url= null;
    this.username = null;
    this.credential = null;
}

function MethodExecution() {
    this.handler = null;
    this.method = null;
    this.args = null;
}

function DataMessage() {
    this.t = null;
    this.sn = null;
    this.r = null;
    this.id = null;
    this.data = null;
}

// --- auth objects ---

function AuthResponse() {
    this.status = null;
    this.user = null;
}

function User() {
    this.id = null;
    this.username = null;
    this.visibleName =  null;
    this.textStatus = null;
    this.status = null;
    this.userAgent = null;
    this.chatlog = null;
}

Participant = function(username){
    this.username = username;
    this.role = null;
};

// --- conversation objects ---

ConversationInfo = function(){
    this.id = null;
    this.name = null;
    this.participants = [];
    this.attributes = null;
};

Message = function() {
    this.id = null;
    this.from = null;
    this.body = null;
    //this.status = null;
};

MessageBody = function(text) {
    this.text = text;
};

// --- call objects ---

function CallInfo() {
    this.id = null;
    this.participants = null;
    this.attributes = null;
    this.connectionInfo = null;
    this.destructAlgorithm = null;
    this.p2p = null;
}

function ConnectionInfo(){
    this.usersSessionInfo = null;
    this.sessionInfo = null;
    this.p2p = null;
}

function ExtraConnectionInfo(){
    this.extraSDPInfo = null;
    this.usersExtraSDPInfo = null;
}

function SessionInfo(){
    this.connectionId = null;
    this.sdpInfo = null;
}

function SDPInfo(){
    this.sdp = null;
    this.type = null;
}

function ExtraSDPInfo() {
    this.iceCandidates = [];
}
