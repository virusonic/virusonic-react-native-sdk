var VSEvent = function () {
};

VSEvent.onUsersChange = 'onUsersChange';
VSEvent.onCalls = 'onCalls';
VSEvent.invite = 'invite';
VSEvent.joined = 'joined';
VSEvent.left = 'left';
VSEvent.finished = 'finished';
VSEvent.onTrack = 'onTrack';



var APIEvent = function () {
};

APIEvent['onUsersChange'] = 'onUsersChange';
APIEvent['invite'] = 'invite';
APIEvent['joined'] = 'joined';
APIEvent['addedParticipant'] = 'addedParticipant';
APIEvent['removedParticipant'] = 'removedParticipant';
APIEvent['left'] = 'left';
APIEvent['trackInfo'] = 'trackInfo';
APIEvent['finished'] = 'finished';
APIEvent['onTrack'] = 'onTrack';
APIEvent['onCalls'] = 'onCalls';
APIEvent['connectionState'] = 'connectionState';

window['APIEvent'] = APIEvent;

var SessionInfoType = function () {
};
SessionInfoType.offer = 'offer';
SessionInfoType.answer = 'answer';


var ServerMethod = function () {
};
ServerMethod.auth = 'auth';
ServerMethod.getUsers = 'getUsers';
ServerMethod.getCalls = 'getCalls';
ServerMethod.changeSettings = 'changeSettings';
ServerMethod.changePassword = 'changePassword';

ServerMethod.invite = 'invite';
ServerMethod.join = 'join';
ServerMethod.addParticipant = 'addParticipant';
ServerMethod.removeParticipant = 'removeParticipant';
ServerMethod.setTrackInfo = 'setTrackInfo';
ServerMethod.addExtraConnectionInfo = 'addExtraConnectionInfo';
ServerMethod.setConnectionInfo = 'setConnectionInfo';
ServerMethod.leave = 'leave';

ServerMethod.sendMessage = 'sendMessage';

var WorkerEvent = function () {
};
WorkerEvent.iceCandidate = 'w1';
WorkerEvent.onTrack = 'w2';
WorkerEvent.connectionState = 'w3';
WorkerEvent.trackInfo = 'w4';


var CallEvent = function () {
};
CallEvent.onIceCandidate = 'c2';
CallEvent.onTrack = 'c3';
CallEvent.connectionState = 'c4';
CallEvent.trackInfo = 'c5';
CallEvent.dead = 'c6';
CallEvent.finished = 'c7';

ConversationEvent = function(){};
ConversationEvent.created = 'conversation.created';
ConversationEvent.addedMessage = 'conversation.addedMessage';

// VS['ConversationEvent'] = VS.ConversationEvent;
// VS.ConversationEvent['created'] = 'conversation.created';
// VS.ConversationEvent['addedMessage'] = 'conversation.addedMessage';


IConversationEvent = function(){};
IConversationEvent.created = 'conversation.created';
IConversationEvent.addedMessage = 'conversation.addedMessage';

// VS.IConversationEvent.created = 'conversation.created';
// VS.IConversationEvent.addedMessage = 'conversation.addedMessage';

RelationEvent = function() {};
RelationEvent.connected = 'relationEvent.connected';
RelationEvent.disconnected = 'relationEvent.disconnected';

ConnectionState = function() {};
ConnectionState.new = 'connectionState.new';
ConnectionState.connecting = 'connectionState.connecting';
ConnectionState.connected = 'connectionState.connected';
ConnectionState.failed = 'connectionState.failed';
ConnectionState.disconnected = 'connectionState.disconnected';
ConnectionState.completed = 'connectionState.completed';

module.exports = {
    CallEvent:CallEvent,
    WorkerEvent:WorkerEvent,
    ServerMethod:ServerMethod,
    SessionInfoType:SessionInfoType,
    APIEvent:APIEvent,
    VSEvent:VSEvent,
    ConversationEvent: ConversationEvent,
    IConversationEvent: IConversationEvent,
    RelationEvent: RelationEvent,
    ConnectionState: ConnectionState,
};
