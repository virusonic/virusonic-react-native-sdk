EventBus = function(){
    this._listeners = {};
};
//todo create warnings if have several listeners on on event
EventBus.prototype = {
    register: function (event, listener, context) {
        this._listeners[event] = {func: listener, context: context};
    },

    post: function (event, argsArray) {
        var listener = this._listeners[event];
        if (listener) {
            return listener.func.apply(listener.context ? listener.context : window, argsArray);
        }
    },

    clear: function () {
        this._listeners = {};
    }
};

module.exports = EventBus;
