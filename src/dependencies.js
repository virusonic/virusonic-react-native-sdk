let ReactNativeWebRTC = {};

try {
  ReactNativeWebRTC = require('react-native-webrtc');
} catch(e) {
  console.error("Can not load React-native-webrtc", e);
  ReactNativeWebRTC = {};
}

const {
  RTCPeerConnection,
  RTCIceCandidate,
  RTCSessionDescription,
  MediaStream,
  mediaDevices
} = ReactNativeWebRTC;

module.exports = {
  RTCPeerConnection: RTCPeerConnection,
  RTCSessionDescription: RTCSessionDescription,
  RTCIceCandidate: RTCIceCandidate,
  MediaStream: MediaStream,
  mediaDevices: mediaDevices
}
