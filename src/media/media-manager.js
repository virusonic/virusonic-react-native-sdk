const mediaDevices = require('../dependencies').mediaDevices;
const MediaStream = require('../dependencies').MediaStream;

//todo implement save tracks as constraint width, height
var LocalMediaManager = function LocalMediaManager() {
    this.audioTrack = null;
    this.videoTrack = null;
}

LocalMediaManager.prototype.clear = function () {
    this.audioTrack = null;
    this.videoTrack = null;
    //todo stop tracks
};

LocalMediaManager.prototype.available = function () {
    return getUserMedia !== undefined && RTCPeerConnection !== undefined;
};

LocalMediaManager.prototype.getStream = function (constraints) {
    const me = this;
    const needConstraints = {};

    const mediaStream = new MediaStream();

    if (constraints.audio) {
        if (me._checkAliveTrack(me.audioTrack)) {
            mediaStream.addTrack(me.audioTrack);
            me.audioTrack.enabled = true;
        } else {
            me.audioTrack = null;
            needConstraints.audio = true;
        }
    }
    if (constraints.video) {
        if (me._checkAliveTrack(me.videoTrack)) {
            mediaStream.addTrack(me.videoTrack);
            me.videoTrack.enabled = true;
        } else {
            me.videoTrack = null;
            needConstraints.video = true;
        }
    }
    if (needConstraints.video || needConstraints.audio) {
        return mediaDevices.getUserMedia(needConstraints).then(stream => {
            if (needConstraints.audio) {
                me.audioTrack = stream.getAudioTracks()[0];
                mediaStream.addTrack(me.audioTrack);
            }
            if (needConstraints.video) {
                me.videoTrack = stream.getVideoTracks()[0];
                mediaStream.addTrack(me.videoTrack);
            }
            return Promise.resolve(mediaStream);
        });
    } else {
        return Promise.resolve(mediaStream);
    }
};

LocalMediaManager.prototype._checkAliveTrack = function (track) {
    return track && track.readyState === "live";
}

module.exports = LocalMediaManager;
