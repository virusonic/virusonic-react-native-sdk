import rnWebrtc from 'react-native-webrtc';

export const mediaDevices = rnWebrtc.mediaDevices;
export const RTCPeerConnection = rnWebrtc.RTCPeerConnection;
export const RTCSessionDescription =  rnWebrtc.RTCSessionDescription;
export const RTCIceCandidate =  rnWebrtc.RTCIceCandidate;
export const MediaStream =  rnWebrtc.MediaStream;

