var EventBus = require('../util/event-bus');
var VSSON = require('../api/vsson/response-callback');
var Constants = require('../util/datatype');
var UserManager = function UserManager(client, options) {
    EventBus.apply(this, arguments);
    this._client = client;
    this._options = options;
    this._users = {};
};

UserManager.prototype = Object.create(EventBus.prototype);
UserManager.prototype.constructor = UserManager;

UserManager.prototype.getUsers = function (callbackSetUsers) {
    var me = this;
    var responseCallbackFn = new VSSON.ResponseCallback(function (data) {
        data.forEach(function (user) {
            me._users[user.username] = user;
        });
        for (var x in me._users) {
            var user = me._users[x];
            user.chatlog = [];
        }
        return callbackSetUsers(me._users);
    });
    me._client.call("main", Constants.ServerMethod.getUsers, [], responseCallbackFn);
};


UserManager.prototype.callback_updateUsers = function (userArray) {
    this.notify(Constants.VSEvent.onUsersChange, [userArray]);
};

// VS['UserManager'] = VS.UserManager;
// VS.UserManager.prototype['getUsers'] = VS.UserManager.prototype.getUsers;


module.exports = UserManager;